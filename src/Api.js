import fetch from 'node-fetch'

const fetchPatchData = (url, dataPost) => {
  return fetch(`${url}`, {
    method: 'PATCH', // or 'PUT'
    body: JSON.stringify(dataPost), // data can be `string` or {object}!
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((res) => res.json())
    .catch((error) => error)
}

const fetchPatchDataAuth = (url, dataPost, token) => {
  return fetch(`${url}`, {
    method: 'PATCH', // or 'PUT'
    body: JSON.stringify(dataPost), // data can be `string` or {object}!
    headers: {
      'Content-Type': 'application/json',
      Authorization: token,
    },
  })
    .then((res) => res.json())
    .catch((error) => error)
}

const fetchPostData = (url, dataPost) => {
  return fetch(`${url}`, {
    method: 'POST', // or 'PUT'
    body: JSON.stringify(dataPost),
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((res) => res.json())
    .catch((error) => error)
}

const fetchPostDataAuth = (url, dataPost, token) => {
  return fetch(`${url}`, {
    method: 'POST', // or 'PUT'
    body: JSON.stringify(dataPost),
    headers: {
      'Content-Type': 'application/json',
      Authorization: token,
    },
  })
    .then((res) => res.json())
    .catch((error) => error)
}

const fetchDeleteAuth = (url, dataDelete, token) => {
  return fetch(`${url}`, {
    method: 'DELETE', // or 'PUT'
    body: JSON.stringify(dataDelete),
    headers: {
      'Content-Type': 'application/json',
      Authorization: token,
    },
  })
    .then((res) => res.json())
    .catch((error) => error)
}

const fetchGetData = (url) => {
  return fetch(`${url}`, {
    method: 'GET',
  })
    .then((res) => {
      console.log('API')
      console.log(res)
      return res.json()
    })
    .catch((error) => error)
}

const fetchGetAuth = (url, token) => {
  return fetch(`${url}`, {
    method: 'GET',
    headers: {
      Authorization: token,
    },
  })
    .then((res) => res.json())
    .catch((error) => error)
}

module.exports = {
  fetchPatchData,
  fetchPatchDataAuth,
  fetchGetData,
  fetchGetAuth,
  fetchPostData,
  fetchDeleteAuth,
  fetchPostDataAuth,
}
